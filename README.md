# composer/[composer](https://packagist.org/packages/composer/composer)

Dependency Manager for PHP https://packagist.org/packages/composer/composer

[![PHPPackages Rank](http://phppackages.org/p/composer/composer/badge/rank.svg)](http://phppackages.org/p/composer/composer)
[![PHPPackages Referenced By](http://phppackages.org/p/composer/composer/badge/referenced-by.svg)](http://phppackages.org/p/composer/composer)

* https://getcomposer.org

## Official documentation
* [*Composer 1.10: composer fund*](https://blog.packagist.com/composer-fund/)
  2020-03 Nils Adermann
* [*Scripts*](https://getcomposer.org/doc/articles/scripts.md)

## Unofficial documentation
### Cheatsheets
* [*composer cheatsheet*](https://devhints.io/composer)

### Advice
* [*24 Tips for Using Composer Efficiently*
  ](https://blog.martinhujer.cz/17-tips-for-using-composer-efficiently/)
  2018-01 - 2019-04 Martin Hujer
* [*Have you tried Composer Scripts? You may not need Phing.*
  ](https://blog.martinhujer.cz/have-you-tried-composer-scripts/)
  2018-01 Martin Hujer
  * [![PHPPackages Rank](http://phppackages.org/p/phing/phing/badge/rank.svg)](http://phppackages.org/p/phing/phing)
    [![PHPPackages Referenced By](http://phppackages.org/p/phing/phing/badge/referenced-by.svg)](http://phppackages.org/p/phing/phing)
    phing/[phing](https://packagist.org/packages/phing/phing)
  * https://gitlab.com/php-packages-demo/phing
* [*use function doesn't import functions in PHP*
  ](https://stackoverflow.com/questions/51547511/use-function-doesnt-import-functions-in-php)
  (2018)

## Related projects
* [![PHPPackages Rank](http://phppackages.org/p/maglnet/composer-require-checker/badge/rank.svg)](http://phppackages.org/p/maglnet/composer-require-checker)
  [![PHPPackages Referenced By](http://phppackages.org/p/maglnet/composer-require-checker/badge/referenced-by.svg)](http://phppackages.org/p/maglnet/composer-require-checker)
  maglnet/[composer-require-checker](https://packagist.org/packages/maglnet/composer-require-checker)
  * [![PHPPackages Rank](http://phppackages.org/p/slevomat/composer-require-checker-shim/badge/rank.svg)](http://phppackages.org/p/slevomat/composer-require-checker-shim)
    [![PHPPackages Referenced By](http://phppackages.org/p/slevomat/composer-require-checker-shim/badge/referenced-by.svg)](http://phppackages.org/p/slevomat/composer-require-checker-shim)
    slevomat/[composer-require-checker-shim](https://packagist.org/packages/slevomat/composer-require-checker-shim)
  * [![PHPPackages Rank](http://phppackages.org/p/irstea/composer-require-checker-shim/badge/rank.svg)](http://phppackages.org/p/irstea/composer-require-checker-shim)
    [![PHPPackages Referenced By](http://phppackages.org/p/irstea/composer-require-checker-shim/badge/referenced-by.svg)](http://phppackages.org/p/irstea/composer-require-checker-shim)
    irstea/[composer-require-checker-shim](https://packagist.org/packages/irstea/composer-require-checker-shim)
* [![PHPPackages Rank](http://phppackages.org/p/consolidation/cgr/badge/rank.svg)](http://phppackages.org/p/consolidation/cgr)
  [![PHPPackages Referenced By](http://phppackages.org/p/consolidation/cgr/badge/referenced-by.svg)](http://phppackages.org/p/consolidation/cgr)
  consolidation/[cgr](https://packagist.org/packages/consolidation/cgr)