<?php

copy('https://getcomposer.org/installer', 'composer-setup.php');
# In case of error?

$signature = file_get_contents('https://composer.github.io/installer.sig');

if (false === $signature):
    exit('Signature download error');
endif;

if (trim($signature) !== hash_file('SHA384', 'composer-setup.php')):
    exit('Hash error');
endif;

echo shell_exec(PHP_BINARY.' composer-setup.php --quiet');
if (symlink ('composer.phar', 'composer')):
    unlink('composer-setup.php');
endif;

?>
